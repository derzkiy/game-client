import React, { Component } from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { connect } from 'react-redux'
import { loginUser } from '../src/App/actions'
import Login from './App/layout/components/Login'
import Navbar from './App/components/Navbar'

const { Header, Content, Footer } = Layout;

export default class App extends Component {
  render() {
    return <Layout className="layout">
        <Header>
            <Login/>
        </Header>
        <Content style={{ padding: '0 50px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>Content</div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
            Simple game
        </Footer>
    </Layout>;
  }
}