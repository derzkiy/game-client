import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
import {BrowserRouter} from 'react-router-dom'
import gameApp from './App/reducers'
import './index.css'
import App from './App/layout/containers/App'
import api from './App/middleware/api'
import registerServiceWorker from './registerServiceWorker'
import './App.css';

let createStoreWithMiddleware = applyMiddleware(thunkMiddleware, api, logger)(createStore)

let store = createStoreWithMiddleware(gameApp)

let rootElement = document.getElementById('root')

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>,
    rootElement
);
registerServiceWorker();
