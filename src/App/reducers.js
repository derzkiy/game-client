import { combineReducers } from 'redux'
import LoginConstants from './layout/constants/LoginConstants'
import LogoutConstants from './layout/constants/LogoutConstants'
import RegistrationConstants from './pages/registration/constants/RegistrationConstants'

function auth(state = {
    isFetching: false,
    isAuthenticated: !!localStorage.getItem('token')
}, action) {
    switch (action.type) {
        case LoginConstants.LOGIN:
            return Object.assign({}, state, {
                isFetching: true,
                isAuthenticated: false
            })
        case LoginConstants.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                isAuthenticated: true,
                errorMessage: ''
            })
        case LoginConstants.LOGIN_FAILURE:
            return Object.assign({}, state, {
                isFetching: false,
                isAuthenticated: false,
                errorMessage: action.message
            })
        case LogoutConstants.LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                isFetching: true,
                isAuthenticated: false
            })
        default:
            return state
    }
}

function registration(state = {}, action) {
    switch (action.type) {
        case RegistrationConstants.REGISTER_SUCCESS:
            return Object.assign({}, state, {
                user: action.user
            })
        case RegistrationConstants.REGISTER_FAILURE:
            return Object.assign({}, state, {
                errorMessage: action.error
            })
        default:
            return state
    }
}

const gameApp = combineReducers({
    auth,
    registration
})

export default gameApp