import LoginConstants from './layout/constants/LoginConstants'
import LogoutConstants from './layout/constants/LogoutConstants'
import RegistrationConstants from './pages/registration/constants/RegistrationConstants'
import { CALL_API } from './middleware/api'

function login(creds) {
    return {
        type: LoginConstants.LOGIN,
        isFetching: true,
        isAuthenticated: true
    }
}

function receiveLogin(user) {
    return {
        type: LoginConstants.LOGIN_SUCCESS,
        isFetching: false,
        isAuthenticated: true,
        id_token: user.token
    }
}

function errorLogin(message) {
    return {
        type: LoginConstants.LOGIN_FAILURE,
        isFetching: false,
        isAuthenticated: false,
        message
    }
}

export function loginUser(creds) {

    let config = {
        method: 'POST',
        headers: {
            'Content-Type':'application/x-www-form-urlencoded',
            'Access-Control-Allow-Origin': LoginConstants.BASE_URL,
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
            'Access-Control-Allow-Credentials': true
        },
        body: `email=${creds.email}&password=${creds.password}`
    }

    return dispatch => {
        dispatch(login(creds))

        return fetch(LoginConstants.LOGIN_URL, config)
            .then(response => response.json().then(user => ({user, response})))
            .then(( {user, response} ) =>  {
                console.log(user, response.status);
                if (!response.ok) {
                    dispatch(errorLogin(user.error))
                    return Promise.reject(user)
                } else {
                    localStorage.setItem('id_token', user.id_token)
                    dispatch(receiveLogin(user))
                }
            }).catch((err) => {
                console.log('catch', err)
            })
    }
}

function logout() {
    return {
        type: LogoutConstants.LOGOUT,
        isFetching: true,
        isAuthenticated: true
    }
}

function receiveLogout() {
    return {
        type: LogoutConstants.LOGOUT_SUCCESS,
        isFetching: false,
        isAuthenticated: false
    }
}

export function logoutUser() {
    return dispatch => {
        dispatch(logout())
        localStorage.removeItem('id_token')
        dispatch(receiveLogout())
    }
}

export function registerUser(user) {
    return {
        [CALL_API]: {
            user: user,
            endpoint: '/registration',
            authenticated: false,
            types: [RegistrationConstants.REGISTER, RegistrationConstants.REGISTER_SUCCESS, RegistrationConstants.REGISTER_FAILURE]
        }
    }
}