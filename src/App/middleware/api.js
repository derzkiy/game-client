import options from './../config/parameters';

function callApi(endpoint, authenticated, user) {
    let token = localStorage.getItem('token');
    let config = {};

    console.log('api');

    if(authenticated) {
        if(token) {
            config = {
                headers: { 'Authorization': `Bearer ${token}` }
            }
        }
        else {
            throw new Error("No token saved!");
        }
    }
    if(user) {
        config = {
            method: 'POST',
            body: `email=${user.email}&password=${user.password}&first_name=${user.first_name}&last_name=${user.last_name}`,
            headers: {
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
    }

    return fetch(options.BASE_URL + endpoint, config)
        .then(response =>
            response.json().then(text => ({ text, response }))
        ).then(({ text, response }) => {
            if (!response.ok) {
                return Promise.reject(text)
            }

            return text
        })
}

export const CALL_API = 'GAME_SERVER_API';

export default store => next => action => {

    const callAPI = action[CALL_API];

    // So the middleware doesn't get applied to every single action
    if (typeof callAPI === 'undefined') {
        return next(action)
    }

    let { endpoint, types, authenticated, user } = callAPI;

    const [ successType, errorType ] = types;

    // Passing the authenticated boolean back in our data will let us distinguish between normal and secret quotes
    return callApi(endpoint, authenticated, user).then(
        response =>
            next({
                response,
                authenticated,
                user,
                type: successType
            }),
        error => {
            console.log(error.message)
            return next({
            error: error.message || 'There was an error.',
            user,
            type: errorType
        })}
    )
}