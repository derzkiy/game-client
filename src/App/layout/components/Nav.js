import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {Menu, Icon} from 'antd'

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

class Nav extends Component
{
    state = {
        current: 'main',
    }

    handleClick = (e) => {
        console.log('click ', e);
        this.setState({
            current: e.key,
        });
    }

    render() {
        return <div >
            <Menu
                onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="horizontal"
                theme="dark"
                className="layout-components-nav"
            >
                <SubMenu title={<Icon type="menu-fold" />}>
                    <MenuItemGroup title="">
                        <Menu.Item key="setting:1"><Link to='/'>Home</Link></Menu.Item>
                        <Menu.Item key="setting:2"><Link to='/profile'>Profile</Link></Menu.Item>
                        <Menu.Item key="setting:3"><Link to='/game'>Game</Link></Menu.Item>
                        <Menu.Item key="setting:4"><Link to='/registration'>Registration</Link></Menu.Item>
                    </MenuItemGroup>
                </SubMenu>
            </Menu>
        </div>
    }
}

export default Nav