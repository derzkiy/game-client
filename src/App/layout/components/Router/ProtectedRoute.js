import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Redirect, Route} from 'react-router-dom';

export default class ProtectedRoute extends Component {
    static propTypes = {
        canAccess: PropTypes.bool,
        component: PropTypes.func,
        path: PropTypes.string,
        name: PropTypes.string,
        exact: PropTypes.bool,
        strict: PropTypes.bool
    };

    render() {
        let {canAccess, component, path, name, exact, strict} = this.props;
        let routeProps = {
            path,
            component,
            name,
            exact,
            strict
        };

        return canAccess ? <Route {...routeProps} /> : <Redirect to="/" />
    }
}