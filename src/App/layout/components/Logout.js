import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'antd';

class Logout extends Component {
    render() {
        const { onLogoutClick } = this.props

        return <Button
            onClick={() => onLogoutClick()}
            type="primary"
            htmlType="submit"
            className="layout-component-logout-button"
        >
            Logout
        </Button>
    }
}

Logout.propTypes = {
    onLogoutClick: PropTypes.func.isRequired
}

export default Logout