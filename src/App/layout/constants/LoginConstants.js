import config from '../../config/parameters'
const LoginConstants = {
    BASE_URL: config.BASE_URL,
    LOGIN_URL: config.BASE_URL + '/api/login',
    LOGIN: 'LOGIN',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILURE: 'LOGIN_FAILURE'
}

export default LoginConstants