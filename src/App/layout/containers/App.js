import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom';
import { loginUser, logoutUser } from './../../actions';
import Nav from './../components/Nav';
import Login from './../components/Login';
import Logout from './../components/Logout';
import Home from './../../pages/home/components/Index';
import Profile from './../../pages/profile/components/Index';
import Registration from './../../pages/registration/components/Index';
import Game from './../../pages/game/components/Index';
import Error404 from './../../pages/inner/404/components/Index';
import ProtectedRoute from './../components/Router/ProtectedRoute';

const { Header, Content, Footer } = Layout;

const Main = (isAuthenticated) => {
    return <main>
        <Switch>
            <Route exact path='/' component={Home}/>
            <ProtectedRoute path='/profile' component={Profile} canAccess={isAuthenticated.isAuthenticated}/>
            <ProtectedRoute path='/game' component={Game} canAccess={isAuthenticated.isAuthenticated}/>
            <Route path='/registration' component={Registration}/>
            <Route component={Error404}/>
        </Switch>
    </main>
};


class App extends Component {
    render() {
        const { dispatch, isAuthenticated, errorMessage } = this.props;
        return <Layout className="layout">
            <Header>
                <Nav isAuthenticated={isAuthenticated}/>
                {!isAuthenticated &&
                    <Login
                        errorMessage={errorMessage}
                        onLoginClick={ creds => dispatch(loginUser(creds)) }
                    />
                }

                {isAuthenticated &&
                    <Logout onLogoutClick={() => dispatch(logoutUser())} />
                }
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <Main isAuthenticated={isAuthenticated}/>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
            </Footer>
        </Layout>;
    }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
};

function mapStateToProps(state) {

    const { auth } = state;
    const { isAuthenticated, errorMessage } = auth;

    return {
        isAuthenticated,
        errorMessage
    }
}

export default withRouter(connect(mapStateToProps)(App));