import Form from './../components/_form';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

function mapStateToProps(state) {

    const { registration } = state;
    const { user, errorMessage } = registration;

    return {
        user,
        errorMessage
    }
}

export default withRouter(connect(mapStateToProps)(Form));