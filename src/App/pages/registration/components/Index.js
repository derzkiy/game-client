import React, { Component } from 'react'
import { Breadcrumb } from 'antd'
import Form from './../containers/_form'

class Index extends Component {
    render() {
        //console.log(this.props);
        return <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Registration</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                <h1>Registration</h1>
                <Form/>
            </div>
        </div>
    }
}

export default Index