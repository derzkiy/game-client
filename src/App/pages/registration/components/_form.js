import React, { Component } from 'react'
import { Form, Input, Button, Modal } from 'antd'
import PropTypes from "prop-types";
import { registerUser } from '../../../actions'
const FormItem = Form.Item

class _form extends Component {
    success() {
        const modal = Modal.success({
            title: 'This is a notification message',
            content: 'This modal will be destroyed after 1 second',
        });
        setTimeout(() => modal.destroy(), 1000);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { dispatch } = this.props
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const email = values.email
                const password = values.password
                const first_name = values.first_name
                const last_name = values.last_name
                const user = {
                    email: email.trim(),
                    password: password.trim(),
                    last_name: last_name.trim(),
                    first_name: first_name.trim(),
                }
                dispatch(registerUser(user))
            }
        });
    }

    checkPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    render () {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return <Form onSubmit={this.handleSubmit} className="pages-registration-form">
            <FormItem
                {...formItemLayout}
                label="E-mail"
            >
                {getFieldDecorator('email', {
                    rules: [{
                        type: 'email', message: 'The input is not valid E-mail!',
                    }, {
                        required: true, message: 'Please input your E-mail!',
                    }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Password"
            >
                {getFieldDecorator('password', {
                    rules: [{
                        required: true, message: 'Please input your password!',
                    }, {
                        validator: this.checkConfirm,
                    }],
                })(
                    <Input type="password" />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Confirm Password"
            >
                {getFieldDecorator('confirm', {
                    rules: [{
                        required: true, message: 'Please confirm your password!',
                    }, {
                        validator: this.checkPassword,
                    }],
                })(
                    <Input type="password" onBlur={this.handleConfirmBlur} />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label={(<span>FirstName</span>)}
            >
                {getFieldDecorator('last_name', {
                    rules: [{ required: true, message: 'Please input your last name!', whitespace: true }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label={(<span>Last Name</span>)}
            >
                {getFieldDecorator('first_name', {
                    rules: [{ required: true, message: 'Please input your first name!', whitespace: true }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">Register</Button>
            </FormItem>
        </Form>
    }
}

_form.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
    user: PropTypes.object
}

export default Form.create()(_form);
