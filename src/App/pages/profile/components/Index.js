import React, { Component } from 'react';
import { Breadcrumb } from 'antd';

class Index extends Component {
    render() {
        return <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Profile</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                Profile
            </div>
        </div>
    }
}

export default Index;