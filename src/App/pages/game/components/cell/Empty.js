import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TinyColor from 'tinycolor2';

class Empty extends Component {

    constructor(props) {
        super(props);

        this.state = {
            color: TinyColor(this.props.color)
        };

        this.mouseOut = this.mouseOut.bind(this);
        this.mouseOver = this.mouseOver.bind(this);
    }

    mouseOut() {
        let color = this.state.color;
        this.setState({
            color: color.lighten()
        });
    }

    mouseOver() {
        let color = this.state.color;
        this.setState({
            color: color.darken()
        });
    }

    render() {
        return <div
            className="pages-game-cell"
            style={{background: this.state.color.toString()}}
            onMouseOver={this.mouseOver}
            onMouseOut={this.mouseOut}
            onClick={this.props.onClick}
        />
    }
}

Empty.defaultProps = {
    color: '#f0f2f5'
};

Empty.propTypes = {
    color: PropTypes.string,
    onClick: PropTypes.func
};

export default Empty;