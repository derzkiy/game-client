import React, { Component } from 'react';
import { Button, Divider } from 'antd';
import Cell from './Cell';
import Select from "./cell/Select";

class Field extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cells: [],
            ships: [],
        };

        this.draw = this.draw.bind(this);
        this.saveShip = this.saveShip.bind(this);
        this.select = this.select.bind(this);
        this.validate = this.validate.bind(this);
        this.checkDiagonalCells = this.checkDiagonalCells.bind(this);
        this.checkShipLength = this.checkShipLength.bind(this);
        this.getCellByCoords = this.getCellByCoords.bind(this);
        this.getValidLength = this.getValidLength.bind(this);
        this.getCellsByDirection = this.getCellsByDirection.bind(this);
    }

    componentDidMount() {
        this.setState({
            cells: this.draw(100)
        });
    }

    saveShip() {
        this.state.cells.map((cell) => {
            return false;
        });
    }

    select(cell) {
        let error = this.checkDiagonalCells(cell);
        if (error) {
            return error;
        }
        error = this.checkShipLength(cell);
        console.log(error);

        if (error) {
            return error;
        }

        let cells = this.state.cells;
        cells[cell.props.id].status = !cells[cell.props.id].status ? 1 : 0;
        this.setState({cells:cells});
    }

    validate() {
        this.state.cells.map((cell) => {
            this.checkDiagonalCells(cell.content);
        })
    }

    checkDiagonalCells(cell) {
        let { id, x, y } = cell.props;

        let diagonalCells = [ 
            this.getCellByCoords({x: x-1, y: y-1}),
            this.getCellByCoords({x: x-1, y: y+1}),
            this.getCellByCoords({x: x+1, y: y-1}),
            this.getCellByCoords({x: x+1, y: y+1})
        ];

        let error = false;

        diagonalCells.map((item) => {
            if (item && item.status === 1) {
                error = true;
            }
        });

        return error;
    }

    getCellsByDirection(cell, direction, cells) {
        switch(direction) {
            case 'up':
                let topCell = this.getCellByCoords({x: cell.props.x, y: cell.props.y-1});
                if (topCell && topCell.status) {
                    cells.push(topCell.content);
                    cells.concat(this.getCellsByDirection(topCell.content, direction, cells));
                }
                break;
            case 'right':
                let rightCell = this.getCellByCoords({x: cell.props.x+1, y: cell.props.y});
                if (rightCell && rightCell.status) {
                    cells.push(rightCell.content);
                    cells.concat(this.getCellsByDirection(rightCell.content, direction, cells));
                }
                break;
            case 'down':
                let downCell = this.getCellByCoords({x: cell.props.x, y: cell.props.y+1});
                if (downCell && downCell.status) {
                    cells.push(downCell.content);
                    cells.concat(this.getCellsByDirection(downCell.content, direction, cells));
                }
                break;
            case 'left':
                let leftCell = this.getCellByCoords({x: cell.props.x-1, y: cell.props.y});
                if (leftCell && leftCell.status) {
                    cells.push(leftCell.content);
                    cells.concat(this.getCellsByDirection(leftCell.content, direction, cells));
                }
                break;
        }

        return cells;
    }

    checkShipLength(cell) {
        let cells = [];

        cells.push(cell);

        cells.concat(
            this.getCellsByDirection(cell, 'up', cells),
            this.getCellsByDirection(cell, 'right', cells),
            this.getCellsByDirection(cell, 'down', cells),
            this.getCellsByDirection(cell, 'left', cells)
        );

        let length = this.getValidLength();

        if (cells.length > length) {
            return true;
        }

        return false;
    }

    getCellByCoords(coords) {
        let { x, y } = coords;

        if ( x < 0 || y < 0 || x > 9 || y > 9 ) {
            return null;
        }
        let i = Number(String(y) + String(x));

        return this.state.cells[i];
    }

    getValidLength() {
        return 4;
    }

    draw(count) {
        return Array.from(Array(count).keys()).map((i) => {
            let y = Math.floor(i/10);
            let x = (i % 10);
            return {
                status: 0,
                content: <Cell key={`cell-${x}-${y}`} id={i} x={x} y={y} onSelect={this.select} />
            };
        })
    }

    render() {
        return <div className="pages-game-field">
            { this.state.cells.map((item) => item.content) }
            <div style={{clear: 'both'}}/>
            <Divider/>
            <Button type="primary" onClick={this.saveShip}>Save</Button>
        </div>
    }
}

export default Field;