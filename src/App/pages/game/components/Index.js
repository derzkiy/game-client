import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import Panel from './Panel';

class Index extends Component {
    render() {
        return <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Game</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                <Panel/>
            </div>
        </div>
    }
}

export default Index;