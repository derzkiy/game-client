import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cell from './Cell';

class Ship extends Component {

    constructor(props) {
        super(props);

        this.validate = this.validate.bind(this);
    }

    validate() {
        this.props.cells.map((cell) => {
            console.log(cell);
        })
    }

    render() {
        this.validate();
        return <div/>
    }
}

Ship.defaultProps = {
    cells: [],
    status: 0
};

Ship.propTypes = {
    cells: PropTypes.array,
    status: PropTypes.number,
    validate: PropTypes.func
};

export default Ship;