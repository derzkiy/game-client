import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Empty from './cell/Empty';
import Ship from "./cell/Ship";
import Select from "./cell/Select";
import Error from "./cell/Error";

class Cell extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cell: null
        };

        this.select = this.select.bind(this);
    }

    componentDidMount() {
        this.setState({
            cell: <Empty onClick={this.select}/>
        });
    }

    select() {
        let error = this.props.onSelect(this);
        if (error) {
            if (this.state.cell.type === Empty) {
                this.setState({
                    cell: <Error onClick={this.select}/>
                });
                setTimeout(() => {
                    this.setState({
                        cell: <Empty onClick={this.select}/>
                    });
                }, 1000)
            }
        } else {
            this.setState({
                cell: (this.state.cell.type === Empty) ? <Select onClick={this.select}/> : <Empty onClick={this.select}/>,
            });
        }

    }

    getType() {

    }

    render() {
        return this.state.cell
    }
}

Cell.defaultProps = {
    id: 0,
    x: 0,
    y: 0
};

Cell.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    id: PropTypes.number,
    onSelect: PropTypes.func,
    checkType: PropTypes.func
};

export default Cell;