import React, { Component } from 'react';
import Field from './Field';

class Panel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cells: [],
            ships: [],
        };

        this.addShip = this.addShip.bind(this);
        this.generateField = this.generateField.bind(this);
    }

    generateField() {
    }

    addShip() {
    }

    render() {
        return <div>
            <Field/>
        </div>;
    }
}

export default Panel;