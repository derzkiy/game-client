import React, { Component } from 'react';
import { Breadcrumb } from 'antd';

class Index extends Component {
    render() {
        return <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>404</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                <h3>No match for <code>{this.props.location.pathname}</code></h3>
            </div>
        </div>
    }
}

export default Index;