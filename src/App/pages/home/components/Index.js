import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import TaskTable from './TaskTable';
import ProjectTable from "./ProjectTable";
import ProjectForm from "./ProjectForm";
import TaskForm from "./TaskForm";
import HistoryTable from "./HistoryTable";

class Index extends Component {
    render() {
        return <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
            </div>
        </div>
    }
}

export default Index