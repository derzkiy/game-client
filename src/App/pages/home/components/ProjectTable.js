import React, { Component } from 'react'
import { Table, Divider, Icon, Button } from 'antd'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        render: text => <a href="#">{text}</a>,
    },
    {
        title: 'Tasks',
        dataIndex: 'tasks',
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Create',
        dataIndex: 'activity',
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: () => <div><Icon style={{marginRight: '8px'}} type="edit" /><Icon type="close-circle-o" /></div>
    },
];

const data = [{
    key: '1',
    name: 'example.com',
    description: 'Prod example.com',
    tasks: '4',
    activity: '2018-01-15 00:18:01',
    uuid: 'https://adw.cron.com/9e8f0e12-0fb6-46cc-bb00-6ab14977d70f',
}, {
    key: '2',
    name: 'site.com',
    tasks: '2',
    activity: '2018-01-15 00:18:01',
    uuid: 'https://adw.cron.com/e25fb0bd-8f86-46bd-a6ec-e9ae3c88eed7',
}, {
    key: '3',
    name: 'demo.site.com',
    tasks: '5',
    activity: '2018-01-15 00:08:02',
    uuid: 'https://adw.cron.com/9a73dd09-ec9a-4475-87b7-4a2d93e3d649',
}, {
    key: '4',
    name: 'stag.example.com',
    description: 'Stag example.com',
    tasks: '1',
    activity: '2018-01-15 15:30:01',
    uuid: 'https://adw.cron.com/8ef874a3-4ca2-44bf-b326-1c82cf3f8fff',
}];

class ProjectTable extends Component {
    render() {
        return <div>
            <Button type="primary" style={{float: 'right'}}>
                <Icon type="plus-circle-o" />Add project
            </Button>
            <h2>Projects</h2>
            <Divider/>
            <Table columns={columns} dataSource={data} />

        </div>
    }
}

export default ProjectTable