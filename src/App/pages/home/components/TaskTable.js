import React, { Component } from 'react'
import { Table, Divider, Icon } from 'antd'

const columns = [
    {
        title: 'Status',
        dataIndex: 'status',
    },
    {
        title: 'Period',
        dataIndex: 'period',
    },
    {
        title: 'Name',
        dataIndex: 'name',
        render: text => <a href="#">{text}</a>,
    }, {
        title: 'Uuid',
        dataIndex: 'uuid',
    },
    {
        title: 'Last activity',
        dataIndex: 'activity',
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: () => <a href="#">History</a>
    },
];

const data = [{
    key: '1',
    period: '0 18 * * *',
    status: <Icon type="check-circle" style={{ fontSize: 16, color: '#04b655' }} />,
    name: 'Users email notifications',
    activity: '2018-01-15 00:18:01',
    uuid: 'https://adw.cron.com/9e8f0e12-0fb6-46cc-bb00-6ab14977d70f',
}, {
    key: '2',
    period: '*/20 * * * *',
    status: <Icon type="close-circle" style={{ fontSize: 16, color: '#ff4520' }}/>,
    name: 'Parse network activity',
    activity: 'never',
    uuid: 'https://adw.cron.com/e25fb0bd-8f86-46bd-a6ec-e9ae3c88eed7',
}, {
    key: '3',
    period: '0 8 * * *',
    status: <Icon type="check-circle" style={{ fontSize: 16, color: '#04b655' }}/>,
    name: 'Admin feedback notifications',
    activity: '2018-01-15 00:08:02',
    uuid: 'https://adw.cron.com/9a73dd09-ec9a-4475-87b7-4a2d93e3d649',
}, {
    key: '4',
    period: '*/30 * * * *',
    status: <Icon type="exclamation-circle" style={{ fontSize: 16, color: '#ffac43' }}/>,
    name: 'Checking status',
    activity: '2018-01-15 15:30:01',
    uuid: 'https://adw.cron.com/8ef874a3-4ca2-44bf-b326-1c82cf3f8fff',
}];

class TaskTable extends Component {
    render() {
        return <div>
            <h2>Project name</h2>
            <p>Description</p>
            <Divider/>
            <Table columns={columns} dataSource={data} />
        </div>
    }
}

export default TaskTable