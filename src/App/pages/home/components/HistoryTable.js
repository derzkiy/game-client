import React, { Component } from 'react'
import { Table, Divider, Icon } from 'antd'

const columns = [
    {
        title: 'Start',
        dataIndex: 'start',
    },
    {
        title: 'Stop',
        dataIndex: 'stop',
    },
    {
        title: 'Execution time',
        dataIndex: 'time',
    },
    {
        title: 'Error',
        dataIndex: 'error',
        width: 300
    },
    {
        title: 'Ip',
        dataIndex: 'ip',
        key: 'x',
    },
    {
        title: 'User-Agent',
        dataIndex: 'userAgent',
        key: 'x',
    },
    {
        title: 'Status',
        dataIndex: 'status',
    },
];

const data = [{
    key: '1',
    status: <Icon type="close-circle" style={{ fontSize: 16, color: '#ff4520' }}/>,
    name: 'Users email notifications',
    start: '2018-01-15 00:18:01',
    ip: '127.0.0.1',
    time: '3 min',
    error: 'Uncaught exception \'yii\\web\\NotFoundHttpException\' with message \'Страница не найдена.\' in /data/sites/ilikesberbank.ru/public/releases/20171227062050/vendor/yiisoft/yii2/web/Application.php:114',
    stop: '2018-01-15 00:21:01',
    uuid: 'https://adw.cron.com/9e8f0e12-0fb6-46cc-bb00-6ab14977d70f',
}, {
    key: '2',
    status: <Icon type="check-circle" style={{ fontSize: 16, color: '#04b655' }} />,
    name: 'Parse network activity',
    start: '2018-01-15 00:18:01',
    stop: '2018-01-15 00:28:01',
    time: '10 min',
    ip: '127.0.0.1',
    activity: 'never',
    uuid: 'https://adw.cron.com/e25fb0bd-8f86-46bd-a6ec-e9ae3c88eed7',
}, {
    key: '3',
    status: <Icon type="check-circle" style={{ fontSize: 16, color: '#04b655' }}/>,
    name: 'Admin feedback notifications',
    start: '2018-01-15 00:18:01',
    stop: '2018-01-15 01:18:01',
    time: '1 hour',
    ip: '127.0.0.1',
    activity: '2018-01-15 00:08:02',
    uuid: 'https://adw.cron.com/9a73dd09-ec9a-4475-87b7-4a2d93e3d649',
}, {
    key: '4',
    status: <Icon type="exclamation-circle" style={{ fontSize: 16, color: '#ffac43' }}/>,
    name: 'Checking status',
    start: '2018-01-15 00:18:01',
    ip: '127.0.0.1',
    activity: '2018-01-15 15:30:01',
    uuid: 'https://adw.cron.com/8ef874a3-4ca2-44bf-b326-1c82cf3f8fff',
}];

class HistoryTable extends Component {
    render() {
        return <div>
            <h2>Task name (* * * * *)</h2>
            <h3>Project name</h3>
            <p>Description</p>
            <Divider/>
            <Table columns={columns} dataSource={data} />
        </div>
    }
}

export default HistoryTable